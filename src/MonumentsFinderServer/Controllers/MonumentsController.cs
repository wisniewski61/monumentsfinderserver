﻿using GeoCoordinatePortable;
using Microsoft.AspNetCore.Mvc;
using MonumentsFinderServer.DataAccessLayer;
using MonumentsFinderServer.DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MonumentsFinderServer.Controllers
{
    [Route("api/[controller]")]
    public class MonumentsController: Controller
    {
        private DataContext db = new DataContext();

        [HttpGet]
        public IEnumerable<MonumentModel> get()
        {
            return db.getMonuments().AsEnumerable<MonumentModel>();
        }//get()

        [HttpGet("{id}", Name = "GetMonuments")]
        public MonumentModel get(string id)
        {
            return db.getMonumentById(id);
        }//get()

        [HttpGet]
        [Route("near")]
        public IEnumerable<MonumentModel> GetMonumentsNearCoords(int radius, double latitude, double longitude)
        {
            //TODO: do it in optimal way (in db)
            var allMonuments = db.getMonuments().AsEnumerable<MonumentModel>();
            var pointCoords = new GeoCoordinate(latitude, longitude);
            var ret = new List<MonumentModel>();
            radius = radius * 1000;
            foreach (var monument in allMonuments)
            {
                var monumentCoords = new GeoCoordinate(monument.latitude, monument.longitude);
                double distance = monumentCoords.GetDistanceTo(pointCoords);
                if (distance < radius)
                    ret.Add(monument);            
            }//foreach
            return ret.AsEnumerable();
        }
        
        [HttpPost]
        public IActionResult post([FromBody] MonumentModel monument)
        {
            if(monument==null)
                return BadRequest();

            string[] coordsFull = monument.coordinates.Split(' ');
            monument.latitude = ConvertDegreeAngleToDecimal(coordsFull[0]);
            monument.longitude = ConvertDegreeAngleToDecimal(coordsFull[1]);

            db.addMonument(monument);
            return CreatedAtRoute("GetMonuments", new { id = monument.id }, monument);
        }//post()

        private double ConvertDegreeAngleToDecimal(string degreeAngle)
        {
            string[] splitDegrees = degreeAngle.Split('\u00B0');
            double degrees;
            Double.TryParse(splitDegrees[0], out degrees);

            string[] splitMinutes = splitDegrees[1].Split('′');
            double minutes;
            Double.TryParse(splitMinutes[0], out minutes);

            string[] splitSeconds = splitMinutes[1].Split('″');
            double seconds;
            Double.TryParse(splitSeconds[0], out seconds);

            return ConvertDegreeAngleToDouble(degrees, minutes, seconds);
        }//ConvertDegreeAngleToDecimal()

        // source: https://stackoverflow.com/questions/3249700/convert-degrees-minutes-seconds-to-decimal-coordinates/3249890#3249890
        private double ConvertDegreeAngleToDouble(double degrees, double minutes, double seconds)
        {
            //Decimal degrees = 
            //   whole number of degrees, 
            //   plus minutes divided by 60, 
            //   plus seconds divided by 3600

            return degrees + (minutes / 60) + (seconds / 3600);
        }
    }
}
