﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace MonumentsFinderServer.DataAccessLayer.Models
{
    public class MonumentModel
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string imgSrc { get; set; }
        public string coordinates { get; set; }
        public double longitude { get; set; }
        public double latitude { get; set; }
    }
}
