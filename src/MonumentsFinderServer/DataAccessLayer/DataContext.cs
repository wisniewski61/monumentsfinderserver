﻿using MongoDB.Driver;
using MonumentsFinderServer.DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonumentsFinderServer.DataAccessLayer
{
    public class DataContext
    {
        private static MongoClient client;
        private static IMongoDatabase database;
        private const string monumentsCollectionName = "Monuments";

        public DataContext()
        {
            if(client==null)
                client = new MongoClient("mongodb://localhost:27017");
            if(database==null)
                database = client.GetDatabase("MonumentsDb");
        }//DataContext()

        public IQueryable<MonumentModel> getMonuments()
        {
            return database.GetCollection<MonumentModel>(monumentsCollectionName).AsQueryable();
        }//getMonuments()

        public MonumentModel getMonumentById(string id)
        {
            return database.GetCollection<MonumentModel>(monumentsCollectionName).Find(m => m.id.Equals(id)).FirstOrDefault();
        }//getMonumentById()

        public void addMonument(MonumentModel monument)
        {
            database.GetCollection<MonumentModel>(monumentsCollectionName).InsertOne(monument);
        }//addMonument()
    }
}
